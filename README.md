# RecyclerView-FastScroll


#### 项目介绍
- 项目名称：RecyclerView-FastScroll
- 所属系列：openharmony的第三方组件适配移植
- 功能：一个简单的快速滚动列表控件
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio 2.2 Beta1
- 基线版本：Release 2.0.1

#### 效果演示
![效果演示](img/1.gif "效果演示.gif")
![效果演示](img/2.gif "效果演示.gif")

#### 安装教程
                                
1.在项目根目录下的build.gradle文件中，
 ```gradle
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}
 ```
2.在entry模块的build.gradle文件中，
 ```gradle
 dependencies {
    implementation('com.gitee.chinasoft_ohos:RecyclerView-FastScroll:1.0.0')
    ......  
 }
```

在sdk6，DevEco Studio 2.2 Beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

```xml
<com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView
    ohos:id="$+id:listview"
    ohos:height="match_parent"
    ohos:width="match_parent"
    app:fastScrollPopupBgColor="#FF4081"
    app:fastScrollPopupTextColor="#ffffff"
    app:fastScrollThumbColor="#FF4081" />
```

要显示FastScrollPopup，适配器必须实现FastScrollRecyclerView.SectionedAdapter并重写getSectionName()。
如果你需要知道什么时候快速滚动开始或停止，你可以设置一个OnFastScrollStateChangedListener到FastScrollRecyclerView。

不同高度

默认情况下，FastScrollRecyclerView适配器中的所有项目具有相同的高度。如果您的适配器然后你应该让你的适配器实现MeasurableAdapter接口和
重写getViewTypeHeight() 否则滚动拇指可能不会出现在正确的位置，滚动可能是不一致的。
getViewTypeHeight()返回给定类型的单个视图的高度，以像素为单位。每个视图的高度必须是一个视图类型的所有实例之间是固定不变的。因为实现者负责计算这个值，这不适用于视图类型，其中视图的高度是由
项目所使用的文本行数可变。

定制

```xml
你可以使用xml中的fastScrollAutoHide和fastScrollAutoHideDelay属性来启用/禁用自动隐藏:
<com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView
 app:fastScrollAutoHide="true"
 app:fastScrollAutoHideDelay="1500"
 ...
```
以下可以通过xml样式:
```xml
<com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView
    ohos:id="$+id:listview"
    ohos:height="match_parent"
    ohos:width="match_parent"
    ohos:background_element="#ffffff"
    ohos:layout_alignment="vertical_center"
    app:fastScrollAutoHide="true"
    app:fastScrollAutoHideDelay="1500"
    app:fastScrollEnableThumbInactiveColor="true"
    app:fastScrollPopupBackgroundSize="62vp"
    app:fastScrollPopupBgColor="#FF4081"
    app:fastScrollPopupPosition="adjacent"
    app:fastScrollPopupTextColor="#ffffff"
    app:fastScrollPopupTextSize="32fp"
    app:fastScrollPopupTextVerticalAlignmentMode="font_metrics"
    app:fastScrollThumbColor="#FF4081"
    app:fastScrollThumbEnabled="true"/>
```
#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


#### 版本迭代

- 1.0.0

#### 版权和许可信息

  Apache 2.0