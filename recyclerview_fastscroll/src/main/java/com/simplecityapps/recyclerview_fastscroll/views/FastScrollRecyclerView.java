/*
 * Copyright (c) 2016 Tim Malseed
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.simplecityapps.recyclerview_fastscroll.views;

import com.simplecityapps.recyclerview_fastscroll.utils.TypedAttrUtils;
import com.simplecityapps.recyclerview_fastscroll.interfaces.OnFastScrollStateChangeListener;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.database.DataSetSubscriber;
import ohos.agp.render.Canvas;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.app.Context;
import ohos.multimodalinput.event.TouchEvent;

import java.util.HashMap;

public class FastScrollRecyclerView extends RecyclerView implements Component.TouchEventListener, Component.DrawTask {

    private FastScroller mScrollbar;

    private boolean mFastScrollEnabled = true;

    private float mDownX;
    private float mDownY;
    private float mLastY;

    private HashMap mScrollOffsets;

    private ScrollOffsetInvalidator mScrollOffsetInvalidator;

    private OnFastScrollStateChangeListener mStateChangeListener;

    /**
     * 构造
     *
     * @param context context
     */
    public FastScrollRecyclerView(Context context) {
        super(context);
    }

    /**
     * 构造
     *
     * @param context context
     * @param attrs   attrs
     */
    public FastScrollRecyclerView(Context context, AttrSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    public FastScrollRecyclerView(Context context, AttrSet attrs, String defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttrSet attrs) {
        mFastScrollEnabled = TypedAttrUtils.getBoolean(attrs, "fastScrollThumbEnabled", true);
        mScrollbar = new FastScroller(context, this, attrs);
        mScrollOffsetInvalidator = new ScrollOffsetInvalidator();
        mScrollOffsets = new HashMap();
        addDrawTask(this);
        setTouchEventListener(this);
    }

    public int getScrollBarWidth() {
        return mScrollbar.getWidth();
    }

    public int getScrollBarThumbHeight() {
        return mScrollbar.getThumbHeight();
    }

    public void setAdapter(RecyclerView.Adapter adapter) {
        if (getAdapter() != null) {
            getAdapter().removeDataSubscriber(mScrollOffsetInvalidator);
        }

        if (adapter != null) {
            adapter.addDataSubscriber(mScrollOffsetInvalidator);
        }
        setItemProvider(adapter);
    }

    private RecyclerView.Adapter getAdapter() {
        return (RecyclerView.Adapter) getItemProvider();
    }

    @Override
    public boolean onTouchEvent(Component component, TouchEvent touchEvent) {
        return handleTouchEvent(touchEvent);
    }

    private boolean handleTouchEvent(TouchEvent ev) {
        int action = ev.getAction();
        float x = getTouchX(ev, 0, this);
        float y = getTouchY(ev, 0, this);
        switch (action) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                // Keep track of the down positions
                mDownX = x;
                mDownY = mLastY = y;
                mScrollbar.handleTouchEvent(ev, mDownX, mDownY, mLastY, mStateChangeListener);
                return true;
            case TouchEvent.POINT_MOVE:
                if (mLastY != y && mScrollbar.isDragging()) {
                    scrollBy(0, (int) (-(mLastY - y) / getAvailableScrollBarHeight() * getAvailableScrollHeight((int) getAdapterHeight(), 0)));
                }
                mLastY = y;
                mScrollbar.handleTouchEvent(ev, mDownX, mDownY, mLastY, mStateChangeListener);
                break;
            case TouchEvent.PRIMARY_POINT_UP:
            case TouchEvent.CANCEL:
                mScrollbar.handleTouchEvent(ev, mDownX, mDownY, mLastY, mStateChangeListener);
                break;
        }
        return mScrollbar.isDragging();
    }

    private float getTouchX(TouchEvent event, int index, Component component) {
        float x = 0;
        if (event.getPointerCount() > index) {
            int[] xy = component.getLocationOnScreen();
            if (xy != null && xy.length == 2) {
                x = event.getPointerScreenPosition(index).getX() - xy[0];
            } else {
                x = event.getPointerPosition(index).getX();
            }
        }
        return x;
    }

    private float getTouchY(TouchEvent event, int index, Component component) {
        float y = 0;
        if (event.getPointerCount() > index) {
            int[] xy = component.getLocationOnScreen();
            if (xy != null && xy.length == 2) {
                y = event.getPointerScreenPosition(index).getY() - xy[1];
            } else {
                y = event.getPointerPosition(index).getY();
            }
        }
        return y;
    }

    protected int getAvailableScrollHeight(int adapterHeight, int yOffset) {
        int visibleHeight = getHeight();
        int scrollHeight = getPaddingTop() + yOffset + adapterHeight + getPaddingBottom();
        return scrollHeight - visibleHeight;
    }

    /**
     * Returns the available scroll bar height:
     * AvailableScrollBarHeight = Total height of the visible view - thumb height
     *
     * @return int
     */
    protected int getAvailableScrollBarHeight() {
        int visibleHeight = getHeight() - getPaddingTop() - getPaddingBottom();
        return visibleHeight - mScrollbar.getThumbHeight();
    }


    @Override
    public void onDraw(Component component, Canvas canvas) {
        if (mFastScrollEnabled) {
            onUpdateScrollbar();
            mScrollbar.draw(canvas);
        }
    }

    /**
     * Maps the touch (from 0..1) to the adapter position that should be visible.
     *
     * @param touchFraction1 touchFraction1
     * @param barTop         barTop
     * @return String
     */
    public String scrollToPositionAtProgress(float touchFraction1, float barTop) {
        if (!(getAdapter() instanceof SectionedAdapter)) {
            return "";
        }
        SectionedAdapter sectionedAdapter = (SectionedAdapter) getAdapter();

        if (touchFraction1 == 0) {
            return sectionedAdapter.getSectionName(0);
        }

        int posInt = getPosition(getOffsetY() + barTop);

        return sectionedAdapter.getSectionName(posInt);
    }

    /**
     * Updates the bounds for the scrollbar.
     */
    public void onUpdateScrollbar() {

        if (getAdapter() == null) {
            return;
        }

        int rowCount = getAdapter().getCount();

        // Skip early if, there are no items.
        if (rowCount == 0) {
            mScrollbar.setThumbPosition(-1, -1);
            return;
        }

        int scrollBarX;
        int scrollBarY;
        if (isRtl()) {
            scrollBarX = 0;
        } else {
            scrollBarX = getWidth() - mScrollbar.getWidth();
        }

        scrollBarY = (int) ((getOffsetY() / getAvailableScrollHeight((int) getAdapterHeight(), 0)) * getAvailableScrollBarHeight());
        scrollBarY += getPaddingTop();

        mScrollbar.setThumbPosition(scrollBarX, scrollBarY);
    }

    private float getOffsetY() {
        float height = 0;
        if (getAdapter() == null) {
            return height;
        }
        int rowIndex = getItemPosByVisibleIndex(0);
        Component component = getComponentAt(rowIndex);

        if (rowIndex == 0) {
            return Math.abs(component.getContentPositionY());
        }

        if (getAdapter() instanceof MeasurableAdapter) {
            out:
            for (int i = 0; i < getAdapter().getCount(); i++) {
                if (i == rowIndex) {
                    break out;
                }
                height += ((MeasurableAdapter) getAdapter()).getViewTypeHeight(this, null, getAdapter().getItemViewType(i));

            }
        } else {
            out:
            for (int i = 0; i < getAdapter().getCount(); i++) {
                if (i == rowIndex) {
                    break out;
                }
                height += component.getHeight();
            }
        }

        return height + Math.abs(component.getContentPositionY());
    }

    private float getAdapterHeight() {
        float height = 0;
        if (getAdapter() == null) {
            return height;
        }

        if (getAdapter() instanceof MeasurableAdapter) {
            for (int i = 0; i < getAdapter().getCount(); i++) {
                height += ((MeasurableAdapter) getAdapter()).getViewTypeHeight(this, null, getAdapter().getItemViewType(i));
            }
        } else {
            int rowIndex = getItemPosByVisibleIndex(0);
            Component component = getComponentAt(rowIndex);
            height = getAdapter().getCount() * component.getHeight();
        }

        return height;
    }

    private int getPosition(float scrollHeight) {
        float tempHeight = 0.0F;
        int position = 0;
        int itemHeight = 0;
        if (getAdapter() instanceof MeasurableAdapter) {
            out:
            for (int i = 0; i < getAdapter().getCount(); i++) {
                itemHeight = ((MeasurableAdapter) getAdapter()).getViewTypeHeight(this, null, getAdapter().getItemViewType(i));
                if ((tempHeight + (itemHeight / 2f)) >= scrollHeight || (tempHeight + itemHeight) >= scrollHeight) {
                    position = i;
                    break out;
                } else {
                    tempHeight += itemHeight;
                }
            }
        } else {
            int rowIndex = getItemPosByVisibleIndex(0);
            Component component = getComponentAt(rowIndex);
            out:
            for (int i = 0; i < getAdapter().getCount(); i++) {
                itemHeight = component.getHeight();
                if ((tempHeight + (itemHeight / 2f)) >= scrollHeight || (tempHeight + itemHeight) >= scrollHeight) {
                    position = i;
                    break out;
                } else {
                    tempHeight += itemHeight;
                }
            }
        }
        return position;
    }

    public void showScrollbar() {
        mScrollbar.show();
    }

    public void setThumbColor(Color color) {
        mScrollbar.setThumbColor(color);
    }

    public void setTrackColor(Color color) {
        mScrollbar.setTrackColor(color);
    }

    public void setPopupBgColor(Color color) {
        mScrollbar.setPopupBgColor(color);
    }

    public void setPopupTextColor(Color color) {
        mScrollbar.setPopupTextColor(color);
    }

    public void setPopupTextSize(int textSize) {
        mScrollbar.setPopupTextSize(textSize);
    }

    public void setPopUpTypeface(Font typeface) {
        mScrollbar.setPopupTypeface(typeface);
    }

    public void setAutoHideDelay(int hideDelay) {
        mScrollbar.setAutoHideDelay(hideDelay);
    }

    public void setAutoHideEnabled(boolean autoHideEnabled) {
        mScrollbar.setAutoHideEnabled(autoHideEnabled);
    }

    public void setOnFastScrollStateChangeListener(OnFastScrollStateChangeListener stateChangeListener) {
        mStateChangeListener = stateChangeListener;
    }

    @Deprecated
    public void setStateChangeListener(OnFastScrollStateChangeListener stateChangeListener) {
        setOnFastScrollStateChangeListener(stateChangeListener);
    }

    public void setThumbInactiveColor(Color color) {
        mScrollbar.setThumbInactiveColor(color);
    }

    public void allowThumbInactiveColor(boolean allowInactiveColor) {
        mScrollbar.enableThumbInactiveColor(allowInactiveColor);
    }

    @Deprecated
    public void setThumbInactiveColor(boolean allowInactiveColor) {
        allowThumbInactiveColor(allowInactiveColor);
    }

    public void setFastScrollEnabled(boolean fastScrollEnabled) {
        mFastScrollEnabled = fastScrollEnabled;
    }

    @Deprecated
    public void setThumbEnabled(boolean thumbEnabled) {
        setFastScrollEnabled(thumbEnabled);
    }

    /**
     * Set the FastScroll Popup position. This is either {@link FastScroller.PopupPosition#ADJACENT},
     * meaning the popup moves adjacent to the FastScroll thumb, or {@link FastScroller.PopupPosition#CENTER},
     * meaning the popup is static and centered within the RecyclerView.
     *
     * @param popupPosition popupPosition
     */
    public void setPopupPosition(@FastScroller.PopupPosition int popupPosition) {
        mScrollbar.setPopupPosition(popupPosition);

    }

    private class ScrollOffsetInvalidator extends DataSetSubscriber {
        private void invalidateAllScrollOffsets() {
            mScrollOffsets.clear();
        }

        @Override
        public void onChanged() {
            invalidateAllScrollOffsets();
        }

        @Override
        public void onInvalidated() {
        }

        @Override
        public void onItemChanged(int position) {
            invalidateAllScrollOffsets();
        }

        @Override
        public void onItemInserted(int position) {
            invalidateAllScrollOffsets();
        }

        @Override
        public void onItemRemoved(int position) {
            invalidateAllScrollOffsets();
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount) {
            invalidateAllScrollOffsets();
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            invalidateAllScrollOffsets();
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            invalidateAllScrollOffsets();
        }
    }

    public interface SectionedAdapter {

        String getSectionName(int position);
    }

    /**
     * FastScrollRecyclerView by default assumes that all items in a RecyclerView will have
     * ItemViews with the same heights so that the total height of all views in the RecyclerView
     * can be calculated. If your list uses different view heights, then make your adapter implement
     * this interface.
     */
    public interface MeasurableAdapter {
        /**
         * Gets the height of a specific view type, including item decorations
         *
         * @param recyclerView The recyclerView that this item view will be placed in
         * @param viewHolder   The viewHolder that corresponds to this item view
         * @param viewType     The view type to get the height of
         * @return The height of a single view for the given view type in pixels
         */
        int getViewTypeHeight(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, int viewType);
    }
}
