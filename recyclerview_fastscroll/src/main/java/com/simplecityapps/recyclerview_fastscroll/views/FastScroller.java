/*
 * Copyright (c) 2016 Tim Malseed
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.simplecityapps.recyclerview_fastscroll.views;

import com.simplecityapps.recyclerview_fastscroll.utils.TypedAttrUtils;
import com.simplecityapps.recyclerview_fastscroll.interfaces.OnFastScrollStateChangeListener;
import com.simplecityapps.recyclerview_fastscroll.utils.RectUtils;
import com.simplecityapps.recyclerview_fastscroll.utils.Utils;
import ohos.agp.animation.Animator;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;
import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;
import ohos.eventhandler.InnerEvent;
import ohos.multimodalinput.event.TouchEvent;

import java.lang.annotation.Retention;

import static java.lang.annotation.RetentionPolicy.SOURCE;

@SuppressWarnings("WeakerAccess")
public class FastScroller {
    private static final int DEFAULT_AUTO_HIDE_DELAY = 1500;

    private FastScrollRecyclerView mRecyclerView;
    private FastScrollPopup mPopup;

    private int mThumbHeight;
    private int mThumbWidth;
    private Paint mThumb;

    private Paint mTrack;
    private int mTrackWidth;

    private Rect mTmpRect = new Rect();
    private Rect mInvalidateRect = new Rect();
    private Rect mInvalidateTmpRect = new Rect();

    // The inset is the buffer around which a point will still register as a click on the scrollbar
    private int mTouchInset;

    // This is the offset from the top of the scrollbar when the user first starts touching.  To
    // prevent jumping, this offset is applied as the user scrolls.
    private float mTouchOffset;

    private Point mThumbPosition = new Point(-1, -1);
    private Point mOffset = new Point(0, 0);

    private boolean mIsDragging;

    private AnimatorValue mAutoHideAnimator;
    private boolean mAnimatingShow;
    private int mAutoHideDelay = DEFAULT_AUTO_HIDE_DELAY;
    private boolean mAutoHideEnabled = true;
    private Runnable mHideRunnable;

    private Color mThumbActiveColor;
    private Color mThumbInactiveColor;
    private boolean mThumbInactiveState;

    private int mTouchSlop;
    private EventHandler eventHandler = new EventHandler(EventRunner.getMainEventRunner()) {
        protected void processEvent(InnerEvent event) {
            super.processEvent(event);
        }
    };

    @Retention(SOURCE)
    public @interface PopupTextVerticalAlignmentMode {
        int TEXT_BOUNDS = 0;
        int FONT_METRICS = 1;
    }

    public @interface PopupPosition {
        int ADJACENT = 0;
        int CENTER = 1;
    }

    public FastScroller(Context context, FastScrollRecyclerView recyclerView, AttrSet attrs) {
        mRecyclerView = recyclerView;
        mPopup = new FastScrollPopup(recyclerView);

        mThumbHeight = Utils.toPixels(context, 52);
        mThumbWidth = Utils.toPixels(context, 8);
        mTrackWidth = Utils.toPixels(context, 6);

        mTouchInset = Utils.toPixels(context, -24);

        mThumb = new Paint();
        mThumb.setAntiAlias(true);
        mTrack = new Paint();
        mTrack.setAntiAlias(true);
        mTouchSlop = 8;

        mAutoHideEnabled = TypedAttrUtils.getBoolean(attrs, "fastScrollAutoHide", true);
        mAutoHideDelay = TypedAttrUtils.getInteger(attrs, "fastScrollAutoHideDelay", DEFAULT_AUTO_HIDE_DELAY);
        mThumbInactiveState = TypedAttrUtils.getBoolean(attrs, "fastScrollEnableThumbInactiveColor", true);
        mThumbActiveColor = TypedAttrUtils.getColor(attrs, "fastScrollThumbColor", 0x79000000);
        mThumbInactiveColor = TypedAttrUtils.getColor(attrs, "fastScrollThumbInactiveColor", 0x79000000);

        Color trackColor = TypedAttrUtils.getColor(attrs, "fastScrollTrackColor", 0x28000000);
        Color popupBgColor = TypedAttrUtils.getColor(attrs, "fastScrollPopupBgColor", 0xff000000);
        Color popupTextColor = TypedAttrUtils.getColor(attrs, "fastScrollPopupTextColor", 0xffffffff);
        int popupTextSize = (int) TypedAttrUtils.getDimensionPixelSize(attrs, "fastScrollPopupTextSize", Utils.toScreenPixels(recyclerView.getContext(), 32));
        int popupBackgroundSize = (int) TypedAttrUtils.getDimensionPixelSize(attrs, "fastScrollPopupBackgroundSize", Utils.toPixels(recyclerView.getContext(), 62));
        @PopupTextVerticalAlignmentMode int popupTextVerticalAlignmentMode = TypedAttrUtils.getInteger(attrs, "fastScrollPopupTextVerticalAlignmentMode", PopupTextVerticalAlignmentMode.TEXT_BOUNDS);
        @PopupPosition int popupPosition = TypedAttrUtils.getInteger(attrs, "fastScrollPopupPosition", PopupPosition.ADJACENT);

        mTrack.setColor(trackColor);
        mThumb.setColor(mThumbInactiveState ? mThumbInactiveColor : mThumbActiveColor);
        mPopup.setBgColor(popupBgColor);
        mPopup.setTextColor(popupTextColor);
        mPopup.setTextSize(popupTextSize);
        mPopup.setBackgroundSize(popupBackgroundSize);
        mPopup.setPopupTextVerticalAlignmentMode(popupTextVerticalAlignmentMode);
        mPopup.setPopupPosition(popupPosition);

        mHideRunnable = new Runnable() {
            @Override
            public void run() {
                if (!mIsDragging) {
                    if (mAutoHideAnimator != null) {
                        mAutoHideAnimator.cancel();
                    }

                    mAutoHideAnimator = new AnimatorValue();
                    mAutoHideAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                        @Override
                        public void onUpdate(AnimatorValue animatorValue, float v) {
                            if (mRecyclerView.isRtl()) {
                                setOffsetX((int) (v * -1 * getWidth()));
                            } else {
                                setOffsetX((int) (v * 1 * getWidth()));
                            }
                        }
                    });
                    mAutoHideAnimator.setDuration(200);
                    mAutoHideAnimator.start();
                }
            }
        };

        mRecyclerView.addScrolledListener(new Component.ScrolledListener() {
            @Override
            public void onContentScrolled(Component component, int i, int i1, int i2, int i3) {
                show();
                recyclerView.invalidate();
            }
        });

        if (mAutoHideEnabled) {
            postAutoHideDelayed();
        }
    }

    public int getThumbHeight() {
        return mThumbHeight;
    }

    public int getWidth() {
        return Math.max(mTrackWidth, mThumbWidth);
    }

    public boolean isDragging() {
        return mIsDragging;
    }

    public void handleTouchEvent(TouchEvent ev, float downX, float downY, float lastY,
                                 OnFastScrollStateChangeListener stateChangeListener) {
        int action = ev.getAction();
        float y = getTouchY(ev, 0, mRecyclerView);
        switch (action) {
            case TouchEvent.PRIMARY_POINT_DOWN:
                if (isNearPoint(downX, downY)) {
                    mTouchOffset = downY - mThumbPosition.getPointY();
                    mRecyclerView.setEnabled(false);
                }
                break;
            case TouchEvent.POINT_MOVE:
                // Check if we should start scrolling
                if (!mIsDragging && isNearPoint(downX, downY) &&
                        Math.abs(y - downY) > mTouchSlop) {
                    mIsDragging = true;
                    mTouchOffset += (lastY - downY);
                    mPopup.animateVisibility(true);
                    if (stateChangeListener != null) {
                        stateChangeListener.onFastScrollStart();
                    }
                    if (mThumbInactiveState) {
                        mThumb.setColor(mThumbActiveColor);
                    }
                }

                if(isDragging()){
                    float temp1 = (mThumbPosition.getPointY())/(mRecyclerView.getHeight()-getThumbHeight());
                    String sectionName = mRecyclerView.scrollToPositionAtProgress(temp1,mThumbPosition.getPointY()+getThumbHeight());
                    mPopup.setSectionName(sectionName);
                    mPopup.animateVisibility(!sectionName.isEmpty());
                    mPopup.updateFastScrollerBounds(mRecyclerView, (int) mThumbPosition.getPointY());
                }
                mRecyclerView.invalidate();
                break;
            case TouchEvent.PRIMARY_POINT_UP:
            case TouchEvent.CANCEL:
                mTouchOffset = 0;
                if (mIsDragging) {
                    mIsDragging = false;
                    mPopup.animateVisibility(false);
                    if (stateChangeListener != null) {
                        stateChangeListener.onFastScrollStop();
                    }
                    mRecyclerView.setEnabled(true);
                }
                if (mThumbInactiveState) {
                    mThumb.setColor(mThumbInactiveColor);
                }
                break;
        }
    }

    private float getTouchY(TouchEvent event, int index, Component component) {
        float y = 0;
        if (event.getPointerCount() > index) {
            int[] xy = component.getLocationOnScreen();
            if (xy != null && xy.length == 2) {
                y = event.getPointerScreenPosition(index).getY() - xy[1];
            } else {
                y = event.getPointerPosition(index).getY();
            }
        }
        return y;
    }

    RectFloat rect = new RectFloat();

    public void draw(Canvas canvas) {

        if (mThumbPosition.getPointX() < 0 || mThumbPosition.getPointY() < 0) {
            return;
        }
        //Background
        rect.left = mThumbPosition.getPointX() + mOffset.getPointX() + (mThumbWidth - mTrackWidth);
        rect.top = mOffset.getPointY() + mRecyclerView.getPaddingTop();
        rect.right = mThumbPosition.getPointX() + mOffset.getPointX() + mTrackWidth + (mThumbWidth - mTrackWidth);
        rect.bottom = mRecyclerView.getHeight() + mOffset.getPointY() - mRecyclerView.getPaddingBottom();
        canvas.drawRoundRect(rect,
                mTrackWidth,
                mTrackWidth,
                mTrack);

        //Handle
        rect.left = mThumbPosition.getPointX() + mOffset.getPointX() + (mThumbWidth - mTrackWidth) / 2f;
        rect.top = mThumbPosition.getPointY() + mOffset.getPointY();
        rect.right = mThumbPosition.getPointX() + mOffset.getPointX() + mThumbWidth + (mThumbWidth - mTrackWidth) / 2f;
        rect.bottom = mThumbPosition.getPointY() + mOffset.getPointY() + mThumbHeight;

        canvas.drawRoundRect(rect,
                mThumbWidth,
                mThumbWidth,
                mThumb);


        //Popup
        mPopup.draw(canvas);

    }

    private boolean isNearPoint(float x, float y) {
        mTmpRect.set((int) (mThumbPosition.getPointX()), (int) mThumbPosition.getPointY(), (int) mThumbPosition.getPointX() + mTrackWidth,
                (int) mThumbPosition.getPointY() + mThumbHeight);
        RectUtils.inset(mTmpRect, mTouchInset, mTouchInset);
        return RectUtils.contains(mTmpRect, (int) x, (int) y);
    }

    public void setThumbPosition(int x, int y) {
        if (mThumbPosition.getPointX() == x && mThumbPosition.getPointY() == y) {
            return;
        }
        // do not create new objects here, this is called quite often
        mInvalidateRect.set((int) mThumbPosition.getPointX() + (int) mOffset.getPointX(), (int) mOffset.getPointY(), (int) mThumbPosition.getPointX() + (int) mOffset.getPointX() + mTrackWidth, mRecyclerView.getHeight() + (int) mOffset.getPointY());
        mThumbPosition.modify(x, y);
        mInvalidateTmpRect.set((int) mThumbPosition.getPointX() + (int) mOffset.getPointX(), (int) mOffset.getPointY(), (int) mThumbPosition.getPointX() + (int) mOffset.getPointX() + mTrackWidth, mRecyclerView.getHeight() + (int) mOffset.getPointY());
        Utils.rectUnion(mInvalidateRect, mInvalidateTmpRect);
        mRecyclerView.invalidate();
    }

    public void setOffset(int x, int y) {
        if (mOffset.getPointX() == x && mOffset.getPointY() == y) {
            return;
        }
        // do not create new objects here, this is called quite often
        mInvalidateRect.set((int) mThumbPosition.getPointX() + (int) mOffset.getPointX(), (int) mOffset.getPointY(), (int) mThumbPosition.getPointX() + (int) mOffset.getPointX() + mTrackWidth, mRecyclerView.getHeight() + (int) mOffset.getPointY());
        mOffset.modify(x, y);
        mInvalidateTmpRect.set((int) mThumbPosition.getPointX() + (int) mOffset.getPointX(), (int) mOffset.getPointY(), (int) mThumbPosition.getPointX() + (int) mOffset.getPointX() + mTrackWidth, mRecyclerView.getHeight() + (int) mOffset.getPointY());
        Utils.rectUnion(mInvalidateRect, mInvalidateTmpRect);
        mRecyclerView.invalidate();
    }

    public void setOffsetX(int x) {
        setOffset(x, (int) mOffset.getPointY());
    }

    public int getOffsetX() {
        return (int) mOffset.getPointX();
    }

    public void show() {
        if (!mAnimatingShow && mOffset.getPointX() == getWidth()) {
            if (mAutoHideAnimator != null) {
                mAutoHideAnimator.cancel();
            }

            mAutoHideAnimator = new AnimatorValue();
            mAutoHideAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
                @Override
                public void onUpdate(AnimatorValue animatorValue, float v) {
                    setOffsetX((int) (getWidth() - getWidth() * v));
                }
            });

            mAutoHideAnimator.setStateChangedListener(new Animator.StateChangedListener() {
                @Override
                public void onStart(Animator animator) {

                }

                @Override
                public void onStop(Animator animator) {

                }

                @Override
                public void onCancel(Animator animator) {
                    mAnimatingShow = false;
                }

                @Override
                public void onEnd(Animator animator) {
                    mAnimatingShow = false;
                }

                @Override
                public void onPause(Animator animator) {

                }

                @Override
                public void onResume(Animator animator) {

                }
            });
            mAnimatingShow = true;
            mAutoHideAnimator.setDuration(150);
            mAutoHideAnimator.start();
        }
        if (mAutoHideEnabled) {
            postAutoHideDelayed();
        } else {
            cancelAutoHide();
        }
    }

    protected void postAutoHideDelayed() {
        if (mRecyclerView != null) {
            cancelAutoHide();
            eventHandler.removeTask(mHideRunnable);
            eventHandler.postTask(mHideRunnable, mAutoHideDelay);
        }
    }

    protected void cancelAutoHide() {
        if (mRecyclerView != null) {
            eventHandler.removeTask(mHideRunnable);
        }
    }

    public void setThumbColor(Color color) {
        mThumbActiveColor = color;
        mThumb.setColor(color);
        mRecyclerView.invalidate();
    }

    public void setTrackColor(Color color) {
        mTrack.setColor(color);
        mRecyclerView.invalidate();
    }

    public void setPopupBgColor(Color color) {
        mPopup.setBgColor(color);
    }

    public void setPopupTextColor(Color color) {
        mPopup.setTextColor(color);
    }

    public void setPopupTypeface(Font typeface) {
        mPopup.setTypeface(typeface);
    }

    public void setPopupTextSize(int size) {
        mPopup.setTextSize(size);
    }

    public void setAutoHideDelay(int hideDelay) {
        mAutoHideDelay = hideDelay;
        if (mAutoHideEnabled) {
            postAutoHideDelayed();
        }
    }

    public void setAutoHideEnabled(boolean autoHideEnabled) {
        mAutoHideEnabled = autoHideEnabled;
        if (autoHideEnabled) {
            postAutoHideDelayed();
        } else {
            cancelAutoHide();
        }
    }

    public void setPopupPosition(@PopupPosition int popupPosition) {
        mPopup.setPopupPosition(popupPosition);
    }

    public void setThumbInactiveColor(Color color) {
        mThumbInactiveColor = color;
        enableThumbInactiveColor(true);
    }

    public void enableThumbInactiveColor(boolean enableInactiveColor) {
        mThumbInactiveState = enableInactiveColor;
        mThumb.setColor(mThumbInactiveState ? mThumbInactiveColor : mThumbActiveColor);
    }

    @Deprecated
    public void setThumbInactiveColor(boolean thumbInactiveColor) {
        enableThumbInactiveColor(thumbInactiveColor);
    }
}
