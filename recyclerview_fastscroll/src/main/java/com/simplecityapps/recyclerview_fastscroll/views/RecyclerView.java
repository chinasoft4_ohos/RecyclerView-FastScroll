/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.simplecityapps.recyclerview_fastscroll.views;

import ohos.agp.components.*;
import ohos.app.Context;

public class RecyclerView extends ListContainer {

    public RecyclerView(Context context) {
        super(context);
    }

    public RecyclerView(Context context, AttrSet attrSet) {
        super(context, attrSet);
    }

    public RecyclerView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
    }

    public abstract static class Adapter<VH extends ViewHolder> extends BaseItemProvider {

        public abstract VH onCreateViewHolder(Component parent, int viewType);

        public abstract void onBindViewHolder(ViewHolder holder, int position);

        public abstract int getItemCount();

        public abstract int getItemViewType(int position);

        @Override
        public int getCount() {
            return getItemCount();
        }

        @Override
        public Object getItem(int i) {
            return i;
        }

        @Override
        public long getItemId(int i) {
            return i;
        }

        @Override
        public Component getComponent(int i, Component component, ComponentContainer componentContainer) {
            ViewHolder viewHolder = onCreateViewHolder(componentContainer, getItemViewType(i));
            viewHolder.itemView.setTag(i);
            onBindViewHolder(viewHolder, i);
            return viewHolder.itemView;
        }
    }

    public static class ViewHolder {

        public final Component itemView;

        public ViewHolder(Component itemView) {
            if (itemView == null) {
                throw new IllegalArgumentException("itemView may not be null");
            }
            this.itemView = itemView;

        }
    }
}
