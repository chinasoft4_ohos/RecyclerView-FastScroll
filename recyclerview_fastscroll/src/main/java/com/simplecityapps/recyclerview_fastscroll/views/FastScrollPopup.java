/*
 * Copyright (c) 2016 Tim Malseed
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.simplecityapps.recyclerview_fastscroll.views;

import com.simplecityapps.recyclerview_fastscroll.utils.Utils;
import ohos.agp.animation.AnimatorValue;
import ohos.agp.render.Canvas;
import ohos.agp.render.Paint;
import ohos.agp.render.Path;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.agp.utils.TextTool;

public class FastScrollPopup {

    private FastScrollRecyclerView mRecyclerView;
    private int mBackgroundSize;
    private int mCornerRadius;

    private Path mBackgroundPath = new Path();
    private RectFloat mBackgroundRect = new RectFloat();
    private Paint mBackgroundPaint;

    private Rect mInvalidateRect = new Rect();
    private Rect mTmpRect = new Rect();

    // The absolute bounds of the fast scroller bg
    private Rect mBgBounds = new Rect();

    private String mSectionName;

    private Paint mTextPaint;
    private Rect mTextBounds = new Rect();

    private float mAlpha = 1;

    private AnimatorValue mAlphaAnimator;
    private boolean mVisible;

    @FastScroller.PopupTextVerticalAlignmentMode
    private int mTextVerticalAlignmentMode;
    @FastScroller.PopupPosition
    private int mPosition;

    FastScrollPopup(FastScrollRecyclerView recyclerView) {
        mRecyclerView = recyclerView;

        mBackgroundPaint = new Paint();
        mBackgroundPaint.setAntiAlias(true);
        mTextPaint = new Paint();
        mTextPaint.setAntiAlias(true);
        mTextPaint.setAlpha(0);

        setTextSize(Utils.toScreenPixels(recyclerView.getContext(), 32));
        setBackgroundSize(Utils.toPixels(recyclerView.getContext(), 62));
    }

    public void setBgColor(Color color) {
        mBackgroundPaint.setColor(color);
        mRecyclerView.invalidate();
    }

    public void setTextColor(Color color) {
        mTextPaint.setColor(color);
        mRecyclerView.invalidate();
    }

    public void setTextSize(int size) {
        mTextPaint.setTextSize(size);
        mRecyclerView.invalidate();
    }

    public void setBackgroundSize(int size) {
        mBackgroundSize = size;
        mCornerRadius = mBackgroundSize / 2;
        mRecyclerView.invalidate();
    }

    public void setTypeface(Font typeface) {
        mTextPaint.setFont(typeface);
        mRecyclerView.invalidate();
    }

    public void animateVisibility(boolean visible) {
        if (mVisible != visible) {
            mVisible = visible;
            if (mAlphaAnimator != null) {
                mAlphaAnimator.release();
                mAlphaAnimator.cancel();
            }
            if (visible) {
                setAlpha(1);
            } else {
                setAlpha(0);
            }
//            mAlphaAnimator = new AnimatorValue();
//            mAlphaAnimator.setValueUpdateListener(new AnimatorValue.ValueUpdateListener() {
//                @Override
//                public void onUpdate(AnimatorValue animatorValue, float v) {
//
//                    mRecyclerView.invalidate();
//                }
//            });
//            mAlphaAnimator.setDuration(visible ? 200 : 150);
//            mAlphaAnimator.start();
        }
    }

    // Setter/getter for the popup alpha for animations
    public void setAlpha(float alpha) {
        mAlpha = alpha;
        mRecyclerView.invalidate();
    }

    public float getAlpha() {
        return mAlpha;
    }

    public void setPopupTextVerticalAlignmentMode(@FastScroller.PopupTextVerticalAlignmentMode int mode) {
        mTextVerticalAlignmentMode = mode;
    }

    @FastScroller.PopupTextVerticalAlignmentMode
    public int getPopupTextVerticalAlignmentMode() {
        return mTextVerticalAlignmentMode;
    }

    public void setPopupPosition(@FastScroller.PopupPosition int position) {
        mPosition = position;
    }

    @FastScroller.PopupPosition
    public int getPopupPosition() {
        return mPosition;
    }

    private float[] createRadii() {
        if (mPosition == FastScroller.PopupPosition.CENTER) {
            return new float[]{mCornerRadius, mCornerRadius, mCornerRadius, mCornerRadius, mCornerRadius, mCornerRadius, mCornerRadius, mCornerRadius};
        }

        if (mRecyclerView.isRtl()) {
            return new float[]{mCornerRadius, mCornerRadius, mCornerRadius, mCornerRadius, mCornerRadius, mCornerRadius, 0, 0};
        } else {
            return new float[]{mCornerRadius, mCornerRadius, mCornerRadius, mCornerRadius, 0, 0, mCornerRadius, mCornerRadius};
        }
    }

    public void draw(Canvas canvas) {
        if (isVisible()) {
            // Draw the fast scroller popup
            int restoreCount = canvas.save();
//            canvas.translate(mBgBounds.left, mBgBounds.top);
            mTmpRect.set(mBgBounds.left, mBgBounds.top, mBgBounds.right, mBgBounds.bottom);
            mTmpRect.offset(0, 0);
            mBackgroundPath.reset();
            mBackgroundRect.bottom = mTmpRect.bottom;
            mBackgroundRect.top = mTmpRect.top;
            mBackgroundRect.left = mTmpRect.left;
            mBackgroundRect.right = mTmpRect.right;

            float[] radii = createRadii();
            float baselinePosition;
            if (mTextVerticalAlignmentMode == FastScroller.PopupTextVerticalAlignmentMode.FONT_METRICS) {
                Paint.FontMetrics fontMetrics = mTextPaint.getFontMetrics();
                baselinePosition = (mBgBounds.getHeight() - fontMetrics.ascent - fontMetrics.descent) / 2f;
            } else {
                baselinePosition = (mBgBounds.getHeight() + mTextBounds.getHeight()) / 2f;
            }

            mBackgroundPath.addRoundRect(mBackgroundRect, radii, Path.Direction.CLOCK_WISE);

            mBackgroundPaint.setAlpha(mAlpha);
            mTextPaint.setAlpha((int) (mAlpha * 1));
            canvas.drawPath(mBackgroundPath, mBackgroundPaint);
            canvas.drawText(mTextPaint,
                    mSectionName,
                    (mBgBounds.getWidth() - mTextBounds.getWidth()) / 2f + mBgBounds.left,
                    baselinePosition + mBgBounds.top
            );
            canvas.restoreToCount(restoreCount);
        }
    }

    public void setSectionName(String sectionName) {
        if (!sectionName.equals(mSectionName)) {
            mSectionName = sectionName;
            mTextBounds = mTextPaint.getTextBounds(sectionName);
            // Update the width to use measureText since that is more accurate
            mTextBounds.right = (int) (mTextBounds.left + mTextPaint.measureText(sectionName));
        }
    }

    /**
     * Updates the bounds for the fast scroller.
     *
     * @param recyclerView recyclerView
     * @param thumbOffsetY thumbOffsetY
     * @return Rect
     */
    public Rect updateFastScrollerBounds(FastScrollRecyclerView recyclerView, int thumbOffsetY) {
        mInvalidateRect.set(mBgBounds.left, mBgBounds.top, mBgBounds.right, mBgBounds.bottom);

        if (isVisible()) {
            // Calculate the dimensions and position of the fast scroller popup
            int edgePadding = recyclerView.getScrollBarWidth();
            int bgPadding = Math.round((mBackgroundSize - mTextBounds.getHeight()) / 10f) * 5;
            int bgHeight = mBackgroundSize;
            int bgWidth = Math.max(mBackgroundSize, mTextBounds.getWidth() + (2 * bgPadding));
            if (mPosition == FastScroller.PopupPosition.CENTER) {
                mBgBounds.left = (recyclerView.getWidth() - bgWidth) / 2;
                mBgBounds.right = mBgBounds.left + bgWidth;
                mBgBounds.top = (recyclerView.getHeight() - bgHeight) / 2;
            } else {
                if (recyclerView.isRtl()) {
                    mBgBounds.left = (2 * recyclerView.getScrollBarWidth());
                    mBgBounds.right = mBgBounds.left + bgWidth;
                } else {
                    mBgBounds.right = recyclerView.getWidth() - (2 * recyclerView.getScrollBarWidth());
                    mBgBounds.left = mBgBounds.right - bgWidth;
                }
                mBgBounds.top = recyclerView.getPaddingTop() - recyclerView.getPaddingBottom() + thumbOffsetY - bgHeight + recyclerView.getScrollBarThumbHeight() / 2;
                mBgBounds.top = Math.max(recyclerView.getPaddingTop() + edgePadding, Math.min(mBgBounds.top, recyclerView.getPaddingTop() + recyclerView.getHeight() - edgePadding - bgHeight));
            }
            mBgBounds.bottom = mBgBounds.top + bgHeight;
        } else {
            mBgBounds.clear();
        }

        // Combine the old and new fast scroller bounds to create the full invalidate rect
        Utils.rectUnion(mInvalidateRect, mBgBounds);
        return mInvalidateRect;
    }


    public boolean isVisible() {
        return (mAlpha > 0f) && (!TextTool.isNullOrEmpty(mSectionName));
    }
}
