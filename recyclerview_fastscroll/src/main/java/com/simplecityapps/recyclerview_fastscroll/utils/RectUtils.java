package com.simplecityapps.recyclerview_fastscroll.utils;

import ohos.agp.utils.Rect;

public class RectUtils {

    public static void inset(Rect rect,int dx, int dy) {
        rect.left += dx;
        rect.top += dy;
        rect.right -= dx;
        rect.bottom -= dy;
    }

    public static boolean contains(Rect rect,int x, int y) {
        return rect.left < rect.right && rect.top < rect.bottom  // check for empty first
                && x >= rect.left && x < rect.right && y >= rect.top && y < rect.bottom;
    }

}
