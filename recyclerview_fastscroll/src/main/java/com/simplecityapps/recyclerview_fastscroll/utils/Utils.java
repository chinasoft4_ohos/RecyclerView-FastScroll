/*
 * Copyright (c) 2016 Tim Malseed
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

package com.simplecityapps.recyclerview_fastscroll.utils;

import ohos.agp.components.AttrHelper;
import ohos.agp.utils.Rect;
import ohos.agp.utils.RectFloat;
import ohos.app.Context;

public class Utils {

    /**
     * Converts dp to px
     *
     * @param context Resources
     * @param dp      the value in dp
     * @return int
     */
    public static int toPixels(Context context, float dp) {
        return AttrHelper.vp2px(dp, context);
    }

    /**
     * Converts sp to px
     *
     * @param context Resources
     * @param sp      the value in sp
     * @return int
     */
    public static int toScreenPixels(Context context, float sp) {
        return AttrHelper.fp2px(sp, context);
    }

    public static boolean isRtl() {
        return false;
    }

    public static void rectUnion(Rect one, Rect tow) {
        if ((tow.left < tow.right) && (tow.top < tow.bottom)) {
            if ((one.left < one.right) && (one.top < one.bottom)) {
                if (one.left > tow.left) one.left = tow.left;
                if (one.top > tow.top) one.top = tow.top;
                if (one.right < tow.right) one.right = tow.right;
                if (one.bottom < tow.bottom) one.bottom = tow.bottom;
            } else {
                one.left = tow.left;
                one.top = tow.top;
                one.right = tow.right;
                one.bottom = tow.bottom;
            }
        }
    }

    public static void rectFloatSet(RectFloat one, RectFloat tow) {
        one.bottom = tow.bottom;
        one.top = tow.top;
        one.left = tow.left;
        one.right = tow.right;
    }
}
