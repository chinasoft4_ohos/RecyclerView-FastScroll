/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.simplecityapps.recyclerview_fastscroll.utils;

import ohos.agp.components.Attr;
import ohos.agp.components.AttrSet;
import ohos.agp.components.element.Element;
import ohos.agp.utils.Color;
import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

import java.util.NoSuchElementException;

/**
 * @ClassName: TypedAttrUtils
 * @Description: TypedAttrUtils 工具类
 * @CreateDate: 2021/4/15 15:55
 * @UpdateUser: 更新者
 * @UpdateDate: 2021/4/15 15:55
 * @UpdateRemark: 更新内容
 * @Version: 1.0
 */
public final class TypedAttrUtils {
    static final HiLogLabel LABEL = new HiLogLabel(HiLog.LOG_APP, 0x00201, "zhk");

    /**
     * getLayoutDimension
     *
     * @param attrs    集合
     * @param attrName attrName
     * @param defValue defValue
     * @return int
     */
    public static int getIntColor(AttrSet attrs, String attrName, int defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getColorValue().getValue();
        }
    }

    /**
     * getLayoutDimension
     *
     * @param attrs    集合
     * @param attrName attrName
     * @param defValue defValue
     * @return color
     */
    public static Color getColor(AttrSet attrs, String attrName, Color defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getColorValue();
        }
    }

    /**
     * getColor
     *
     * @param attrs    集合
     * @param attrName attrName
     * @param defValue defValue
     * @return color
     */
    public static Color getColor(AttrSet attrs, String attrName, int defValue) {
        return getColor(attrs, attrName, new Color(defValue));
    }

    /**
     * getLayoutDimension
     *
     * @param attrs    集合
     * @param attrName attrName
     * @param defValue defValue
     * @return int
     */
    public static int getInteger(AttrSet attrs, String attrName, int defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getIntegerValue();
        }
    }

    /**
     * getElement
     *
     * @param attrs    attrs
     * @param attrName attrName
     * @param defValue defValue
     * @return Element
     */
    public static Element getElement(AttrSet attrs, String attrName, Element defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getElement();
        }
    }

    /**
     * getLayoutDimension
     *
     * @param attrs    集合
     * @param attrName attrName
     * @param defValue defValue
     * @return float
     */
    public static float getFloat(AttrSet attrs, String attrName, float defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getFloatValue();
        }
    }

    /**
     * getLayoutDimension
     *
     * @param attrs    集合
     * @param attrName attrName
     * @param defValue defValue
     * @return long
     */
    public static long getLong(AttrSet attrs, String attrName, long defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getLongValue();
        }
    }

    /**
     * getLayoutDimension
     *
     * @param attrs      集合
     * @param attrName   attrName
     * @param isDefValue defValue
     * @return boolean
     */
    public static boolean getBoolean(AttrSet attrs, String attrName, boolean isDefValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return isDefValue;
        } else {
            return attr.getBoolValue();
        }
    }

    /**
     * getLayoutDimension
     *
     * @param attrs    集合
     * @param attrName attrName
     * @param defValue defValue
     * @return String
     */
    public static String getString(AttrSet attrs, String attrName, String defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getStringValue();
        }
    }

    /**
     * getLayoutDimension
     *
     * @param attrs    集合
     * @param attrName attrName
     * @param defValue defValue
     * @return int
     */
    public static float getDimensionPixelSize(AttrSet attrs, String attrName, float defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            return attr.getDimensionValue();
        }
    }

    /**
     * getLayoutDimension
     *
     * @param attrs    集合
     * @param attrName attrName
     * @param defValue defValue
     * @return int
     */
    public static int getLayoutDimension(AttrSet attrs, String attrName, int defValue) {
        Attr attr = attrNoSuchElement(attrs, attrName);
        if (attr == null) {
            return defValue;
        } else {
            HiLog.info(LABEL, "attr.getDimensionValue() = " + attr.getDimensionValue());
            return attr.getDimensionValue();
        }
    }

    private static Attr attrNoSuchElement(AttrSet attrs, String attrName) {
        Attr attr = null;
        try {
            attr = attrs.getAttr(attrName).get();
        } catch (NoSuchElementException e) {
            HiLog.info(LABEL, "Exception = " + e.toString());
        }
        return attr;
    }
}
