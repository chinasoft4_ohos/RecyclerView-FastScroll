package com.simplecityapps.recyclerview_fastscroll.sample;

import ohos.aafwk.ability.delegation.AbilityDelegatorRegistry;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ExampleOhosTest {

    /**
     * 纯UI组件，不涉及单元测试
     */
    @Test
    public void testBundleName() {
        final String actualBundleName = AbilityDelegatorRegistry.getArguments().getTestBundleName();
        assertEquals("com.simplecityapps.recyclerview_fastscroll.sample", actualBundleName);
    }
}