package com.simplecityapps.recyclerview_fastscroll.sample.slice;

import com.simplecityapps.recyclerview_fastscroll.sample.PagerAdapter;
import com.simplecityapps.recyclerview_fastscroll.sample.ResourceTable;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Component;
import ohos.agp.components.PageSlider;
import ohos.agp.components.Text;
import ohos.agp.utils.Color;
import ohos.agp.utils.Point;
import ohos.agp.window.service.Display;
import ohos.agp.window.service.DisplayManager;
import ohos.agp.window.service.WindowManager;

import java.util.Optional;

public class MainAbilitySlice extends AbilitySlice {
    PageSlider pageslider;
    Text text1;
    Text text2;

    private float textLeft1;
    private float textLeft2;

    private float lastV;

    Point pt = new Point();

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        getWindow().setStatusBarColor(Color.getIntColor("#2E409F"));
        getWindow().addWindowFlags(WindowManager.LayoutConfig.MARK_DRAWS_SYSTEM_BAR_BACKGROUNDS);

        pageslider = (PageSlider) findComponentById(ResourceTable.Id_pageslider);
        text1 = (Text) findComponentById(ResourceTable.Id_text1);
        text2 = (Text) findComponentById(ResourceTable.Id_text2);

        Optional<Display>
                display = DisplayManager.getInstance().getDefaultDisplay(MainAbilitySlice.this);
        display.get().getSize(pt);

        text1.setBindStateChangedListener(new Component.BindStateChangedListener() {
            @Override
            public void onComponentBoundToWindow(Component component) {
                textLeft1 = pt.getPointX() / 2f - text1.getWidth() / 2f;
                text1.setTranslationX(textLeft1);
            }

            @Override
            public void onComponentUnboundFromWindow(Component component) {

            }
        });

        text2.setBindStateChangedListener(new Component.BindStateChangedListener() {
            @Override
            public void onComponentBoundToWindow(Component component) {
                textLeft2 = pt.getPointX() - text2.getWidth();
                text2.setTranslationX(textLeft2);
            }

            @Override
            public void onComponentUnboundFromWindow(Component component) {

            }
        });
        pageslider.setProvider(new PagerAdapter(this));
        pageslider.addPageChangedListener(new PageSlider.PageChangedListener() {
            @Override
            public void onPageSliding(int i, float v, int i1) {

                if (v == 0 && i1 == 0) {
                    adjust();
                    return;
                }
                if (i1 < lastV && lastV == 1) {
                    lastV = 1;
                } else {
                    lastV = i1;
                }

                System.out.println("addPageChangedListener onPageSliding " + i + "==" + lastV + "==" + i1);

                if (lastV > 0) {
                    if (v > 0.5) {
                        text1.setTextColor(new Color(Color.getIntColor("#787878")));
                        text2.setTextColor(new Color(Color.getIntColor("#000000")));
                        text1.setTranslationX(0);
                        text2.setTranslationX(textLeft2 - textLeft2 * (v - 0.5f));
                    } else {
                        text1.setTranslationX(textLeft1 - textLeft1 * v * 2);
                        text2.setTranslationX(textLeft2);
                        text1.setTextColor(new Color(Color.getIntColor("#000000")));
                        text2.setTextColor(new Color(Color.getIntColor("#787878")));
                    }
                } else {
                    if (v > 0.5) {
                        text2.setTranslationX(textLeft2);
                        text1.setTranslationX(textLeft1 * (v - 0.5f) * 2);
                        text1.setTextColor(new Color(Color.getIntColor("#000000")));
                        text2.setTextColor(new Color(Color.getIntColor("#787878")));
                    } else {
                        text2.setTranslationX(textLeft1 + textLeft1 * v * 2);
                        text1.setTranslationX(0);
                        text1.setTextColor(new Color(Color.getIntColor("#787878")));
                        text2.setTextColor(new Color(Color.getIntColor("#000000")));
                    }
                }
            }

            @Override
            public void onPageSlideStateChanged(int i) {
                System.out.println("addPageChangedListener onPageSlideStateChanged " + i);
                adjust();
            }

            @Override
            public void onPageChosen(int i) {
                System.out.println("addPageChangedListener onPageChosen " + i);
            }
        });
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }


    private void adjust() {
        if (pageslider.getCurrentPage() == 0) {
            text1.setTranslationX(textLeft1);
            text2.setTranslationX(textLeft2);
        } else {
            text2.setTranslationX(textLeft1);
            text1.setTranslationX(0);
        }
    }
}
