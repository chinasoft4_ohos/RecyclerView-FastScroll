package com.simplecityapps.recyclerview_fastscroll.sample;

import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView;
import com.simplecityapps.recyclerview_fastscroll.views.RecyclerView;
import ohos.agp.components.*;
import ohos.agp.components.element.VectorElement;

public class SimpleRecyclerAdapter extends RecyclerView.Adapter implements FastScrollRecyclerView.SectionedAdapter {

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(Component parent, int viewType) {
        return new ViewHolder(LayoutScatter.getInstance(parent.getContext()).parse(
                ResourceTable.Layout_list_item, null, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.text.setText(String.format("Item %d",position + 1));
    }

    @Override
    public int getItemCount() {
        return 200;
    }

    @Override
    public int getItemViewType(int position) {
        return 0;
    }

    @Override
    public String getSectionName(int position) {
        return String.format("%d", position + 1);
    }


    static class ViewHolder extends RecyclerView.ViewHolder {
        public Text text;
        public Image image;

        ViewHolder(Component itemView) {
            super(itemView);
            text = (Text) itemView.findComponentById(ResourceTable.Id_text);
            image = (Image) itemView.findComponentById(ResourceTable.Id_image);
            VectorElement ele = new VectorElement(itemView.getContext(), ResourceTable.Media_ic_extension_24dp);
            image.setBackground(ele);
        }
    }
}
