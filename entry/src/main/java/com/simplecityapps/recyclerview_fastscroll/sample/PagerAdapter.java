/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.simplecityapps.recyclerview_fastscroll.sample;

import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView;
import ohos.aafwk.ability.AbilitySlice;
import ohos.agp.components.*;

import java.util.HashMap;

public class PagerAdapter extends PageSliderProvider {
    private AbilitySlice abilitySlice;
    HashMap<Integer, DirectionalLayout> cache = new HashMap<>();

    public PagerAdapter(AbilitySlice abilitySlice) {
        this.abilitySlice = abilitySlice;
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public Object createPageInContainer(ComponentContainer componentContainer, int i) {
        if (cache.get(i) == null) {
            DirectionalLayout layout = (DirectionalLayout) LayoutScatter.getInstance(abilitySlice).parse(
                    ResourceTable.Layout_page_item, null, false);
            FastScrollRecyclerView listView = (FastScrollRecyclerView) layout.findComponentById(ResourceTable.Id_listview);
            if (i == 0) {
                listView.setAdapter(new SimpleRecyclerAdapter());
            } else {
                listView.setAdapter(new MultiViewTypeAdapter());
            }
            cache.put(i, layout);
            componentContainer.addComponent(cache.get(i));
        }
        return componentContainer;
    }

    @Override
    public void destroyPageFromContainer(ComponentContainer componentContainer, int i, Object o) {
        componentContainer.removeComponent((Component) o);
    }

    @Override
    public boolean isPageMatchToObject(Component component, Object o) {
        return false;
    }

}
