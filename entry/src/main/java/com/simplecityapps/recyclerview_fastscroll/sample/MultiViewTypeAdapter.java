package com.simplecityapps.recyclerview_fastscroll.sample;

import com.simplecityapps.recyclerview_fastscroll.views.FastScrollRecyclerView;
import com.simplecityapps.recyclerview_fastscroll.views.RecyclerView;
import ohos.agp.components.*;
import ohos.agp.components.element.VectorElement;

public class MultiViewTypeAdapter extends RecyclerView.Adapter
        implements FastScrollRecyclerView.SectionedAdapter,
        FastScrollRecyclerView.MeasurableAdapter {

    @Override
    public ViewHolder onCreateViewHolder(Component parent, int viewType) {
        if (ResourceTable.Layout_list_item == viewType) {
            return new ViewHolder(LayoutScatter.getInstance(parent.getContext()).parse(
                    ResourceTable.Layout_list_item, null, false));
        } else {
            return new ViewHolder(LayoutScatter.getInstance(parent.getContext()).parse(
                    ResourceTable.Layout_list_item_header, null, false));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        viewHolder.text.setText(getNameForItem(position));
    }

    @Override
    public int getItemViewType(int position) {
        if (position % 4 == 0) {
            return ResourceTable.Layout_list_item_header;
        }
        return ResourceTable.Layout_list_item;
    }

    @Override
    public int getItemCount() {
        return 200;
    }

    @Override
    public String getSectionName(int position) {
        if (position % 4 == 0) {
            return String.format("H%d", (position / 4) + 1);
        } else {
            return String.format("%d", position - position / 4);
        }
    }

    public int getViewTypeHeight(RecyclerView recyclerView,
                                 RecyclerView.ViewHolder viewHolder, int viewType) {
        if (viewType == ResourceTable.Layout_list_item_header) {
            return AttrHelper.vp2px(48, recyclerView.getContext());
        } else if (viewType == ResourceTable.Layout_list_item) {
            return AttrHelper.vp2px(92, recyclerView.getContext());
        }
        return 0;
    }

    private String getNameForItem(int position) {
        if (position % 4 == 0) {
            return String.format("Header %d", (position / 4) + 1);
        } else {
            return String.format("Item %d", position - position / 4);
        }
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        public Text text;
        public Image image;

        ViewHolder(Component itemView) {
            super(itemView);
            text = (Text) itemView.findComponentById(ResourceTable.Id_text);
            image = (Image) itemView.findComponentById(ResourceTable.Id_image);
            if (image != null) {
                VectorElement ele = new VectorElement(itemView.getContext(), ResourceTable.Media_ic_extension_24dp);
                image.setBackground(ele);
            }
        }
    }
}
